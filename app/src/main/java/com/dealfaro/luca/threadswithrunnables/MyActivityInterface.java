package com.dealfaro.luca.threadswithrunnables;

/**
 * Created by luca on 9/2/2016.
 */
public interface MyActivityInterface {
    public void displayInteger(int i);
}
