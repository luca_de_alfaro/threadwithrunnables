package com.dealfaro.luca.threadswithrunnables;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements MyActivityInterface {

    // This is our background thread.
    BackgroundThread backgroundThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Creates a background thread
        backgroundThread = new BackgroundThread();
        backgroundThread.setCallback(this);
        backgroundThread.start();
    }

    // Lifecycle.
    @Override
    protected void onStart() {
        super.onStart();
        backgroundThread.onStart();
    }
    @Override
    protected void onStop() {
        backgroundThread.onStop();
        super.onStop();
    }

    public void enterNumber(View v) {
        EditText et = (EditText) findViewById(R.id.editText);
        int i = 0;
        boolean ok = false;
        try {
            i = Integer.parseInt(et.getText().toString());
            ok = true;
        } catch (Throwable t) {};
        // Clears the text.
        et.setText("");
        if (ok && i > 1) {
            Handler handler = backgroundThread.getHandler();
            Message m = handler.obtainMessage(0);
            Bundle data = new Bundle();
            data.putInt("myint", i);
            m.setData(data);
            m.sendToTarget();
        }
    }

    public void displayInteger(int i) {
        // The code here runs in the thread of the caller.
        // We want to modify the UI in the UI thread.
        // We could create a Runnable and post it, or we can take the following shortcut.
        final int ii = i;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView tv = (TextView) findViewById(R.id.textView);
                tv.setText("" + ii);
            }
        });

    }


}
