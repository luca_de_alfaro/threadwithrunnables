package com.dealfaro.luca.threadswithrunnables;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

import java.util.logging.Logger;

/**
 * Created by luca on 9/2/2016.
 */
public class BackgroundThread extends Thread {

    // Looper of the main thread, which we use to send results back.
    // It's not a good habit to call methods of the MainActivity from a
    // background thread, because you don't want code in MainActivity to run
    // in threads other than the UI thread if possible.

    // My handler.
    Handler handler;
    MyActivityInterface ai;

    // The integer on which this thread is working.
    int k;

    // Flag to remember whether to do anything or not.
    boolean is_active;

    public void setCallback(MyActivityInterface ai) {
        this.ai = ai;
    }

    @Override
    public void run() {
        Looper.prepare();
        // We prepare the looper for the current thread.
        // Only the UI thread looper is ready by default.
        handler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 0) {
                    // Let's get the integer from the message.
                    int i = msg.getData().getInt("myint");
                    Log.i("BackgroundThread", "Int is: " + i);
                    // Sets k to it.
                    k = i;
                    // Posts the runnable.
                    removeCallbacks(compute);
                    post(compute);
                }
            }
        };
        Looper.loop();
        is_active = false;
    }

    // Lifecycle.
    public void onStart() {
        is_active = true;
    }
    public void onStop() {
        is_active = false;
    }

    public Handler getHandler() {
        if (handler == null) {
            Log.i("BackgroundThread", "handler is null");
        }
        return handler;
    }

    Runnable compute = new Runnable() {
        @Override
        public void run() {
            // I do things in a silly way just as an example of a computation
            // that takes time.  I am aware I could be doing this by just
            // using postDelayed in the UI thread.  But the point is to show how
            // to accommodate a process that takes time.
            if (k % 2 == 0) {
                k = k / 2;
            } else {
                k = k * 3 + 1;
            }
            try {
                Thread.sleep(1000);
            } catch (Throwable t) {
                t.printStackTrace();
            }
            // And we send k back to the main UI thread.
            // How?  Well, there are two ways.
            // We could call a function of the main activity, that once called,
            // creates a runnable and posts it to its looper.
            // Or, we can create a runnable here, and post it to the UI thread looper.
            // We do the former, because findViewById is not directly available here
            // (there is a way around, but...).
            ai.displayInteger(k);
            // Note how I do just a bit of work in each runnable.  If the runnable takes too long,
            // then we never get to process messages!
            if (k > 1 && is_active) {
                handler.post(this);
            }
        };
    };

}
